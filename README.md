## Description :

Un transfer est un transfert d'argent d'un compte emetteur vers un compte bénéficiaire ...

**Besoin métier :** 

Ajouter un nouveau cas d'usage appelé "Deposit". Le Deposit est un dépôt d'agent cash sur un compte donné (Versement d'argent). 

Imaginez que vous allez à une agence avec un montant de 1000DH et que vous transferez ça en spécifiant le RIB souhaité.
 
L'identifiant fonctionnel d'un compte dans ce cas préçis est le RIB.  


## Travail fait :
```
   le travail demandé est d'ajouter la fonctionnalité deposit ;et pour réaliser ce trvail :
      ## J'ai ajouter:
      ##  MoneyDepositDto qui va etre le data object transfert qu'on utiliser dans l'envoi des donnée.
         * j'ai mapper la classe et sa Dto avec mapStruct.
         * Ajout de MoneyDepositRepository , MoneyDepositService et MoneyDepositServiceImplementation.
         * Ajout de fonctionnalité createDeposit qui va prendre le nom d'emmeteur et rib de compte et faire un depot selon certain critere.
         * Ajout de findByRib au niveau de CompteRepository.
         * Ajout de RestController pour MoneyDeposit où j'ai bien exposer la fonctionnalité clé de cet exercice.
         * Ajout de certaines exceptions.
      * Au niveau d'amélioration de code :
         * j'ai travailler avec la bibliothéque Lombok .
         * suivre une architecture précise : Repository , Service et ServiceImpl.
         * Modifier dans la fonctionnalité de transfer.
      * Au niveau des test :
         * J'ai le test de repositories ; j'ai test findByRib , findByNrCompte.
         * J'ai test createTransfer et createDeposit plus les autres services.
      * Au niveau de sécurité j'ai utiliser une simple authorisation au niveau de quelques api(s); donc pur cette api(s) on entre comme username :user et password : 1234
```
      
       
